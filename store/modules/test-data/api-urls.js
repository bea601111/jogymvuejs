const BASE_URL = '/v1/store-member'

export default {
    DO_LIST: `${BASE_URL}/list`, //get
    DO_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    DO_DETAIL: `${BASE_URL}/detail/id/{id}`, //get
    DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    DO_UPDATE: `${BASE_URL}/{id}`, //put
    DO_DELETE: `${BASE_URL}/{id}`, //del
    DO_CREATE: `${BASE_URL}/data`, //post
    DO_CHECK_USERNAME: `${BASE_URL}/check/user-name`, //get
    DO_CHECK_BUSINESS_NUM: `${BASE_URL}/check/business-number` //get
}
