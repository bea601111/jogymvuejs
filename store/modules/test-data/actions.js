import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store) => {
        return axios.get(apiUrls.DO_LIST)
    },
    [Constants.DO_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_CREATE_COMMENT]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE_COMMENT.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_UPDATE.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },
    [Constants.DO_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE, payload)
    },
    [Constants.DO_CHECK_USERNAME]: (store, payload) => {
        return axios.get(apiUrls.DO_CHECK_USERNAME, payload)
    },
    [Constants.DO_CHECK_BUSINESS_NUM]: (store, payload) => {
        return axios.get(apiUrls.DO_CHECK_BUSINESS_NUM, payload)
    },
}
