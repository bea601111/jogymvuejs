const BASE_URL = '/v1/calculate'

export default {
    DO_LIST: `${BASE_URL}/list/store-member/{storeMemberId}`, //get
    DO_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    DO_DETAIL: `${BASE_URL}/{id}`, //get
    DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    DO_UPDATE: `${BASE_URL}/{id}`, //put
    DO_DELETE: `${BASE_URL}/{id}`, //del
    DO_CREATE: `${BASE_URL}/data/store-member/{storeMemberId}`, //post
}
