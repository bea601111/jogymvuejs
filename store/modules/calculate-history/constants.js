export default {
    DO_LIST: 'calculate-history/doList',
    DO_LIST_PAGING: 'calculate-history/doListPaging',
    DO_DETAIL: 'calculate-history/doDetail',
    DO_CREATE_COMMENT: 'calculate-history/doCreateComment',
    DO_UPDATE: 'calculate-history/doUpdate',
    DO_DELETE: 'calculate-history/doDelete',
    DO_CREATE: 'calculate-history/doCreate'
}
